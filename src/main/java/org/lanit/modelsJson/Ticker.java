package org.lanit.modelsJson;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ticker",
"alerts"
})
@Generated("jsonschema2pojo")
public class Ticker {

@JsonProperty("ticker")
private String ticker;
@JsonProperty("alerts")
private List<Alert> alerts;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("ticker")
public String getTicker() {
return ticker;
}

@JsonProperty("ticker")
public void setTicker(String ticker) {
this.ticker = ticker;
}

@JsonProperty("alerts")
public List<Alert> getAlerts() {
return alerts;
}

@JsonProperty("alerts")
public void setAlerts(List<Alert> alerts) {
this.alerts = alerts;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
