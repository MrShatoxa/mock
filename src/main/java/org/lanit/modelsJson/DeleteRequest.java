package org.lanit.modelsJson;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"info",
"delete",
"uuid",
"lastUpdate"
})
@Generated("jsonschema2pojo")
public class DeleteRequest {

@JsonProperty("info")
private Info info;
@JsonProperty("delete")
private Delete delete;
@JsonProperty("uuid")
private String uuid;
@JsonProperty("lastUpdate")
private String lastUpdate;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("info")
public Info getInfo() {
return info;
}

@JsonProperty("info")
public void setInfo(Info info) {
this.info = info;
}

@JsonProperty("delete")
public Delete getDelete() {
return delete;
}

@JsonProperty("delete")
public void setDelete(Delete delete) {
this.delete = delete;
}

@JsonProperty("uuid")
public String getUuid() {
return uuid;
}

@JsonProperty("uuid")
public void setUuid(String uuid) {
this.uuid = uuid;
}

@JsonProperty("lastUpdate")
public String getLastUpdate() {
return lastUpdate;
}

@JsonProperty("lastUpdate")
public void setLastUpdate(String lastUpdate) {
this.lastUpdate = lastUpdate;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}