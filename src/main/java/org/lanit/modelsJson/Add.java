package org.lanit.modelsJson;


import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"timeFrame",
"percent"
})
@Generated("jsonschema2pojo")
public class Add {

@JsonProperty("name")
private String name;
@JsonProperty("timeFrame")
private Integer timeFrame;
@JsonProperty("percent")
private Integer percent;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("timeFrame")
public Integer getTimeFrame() {
return timeFrame;
}

@JsonProperty("timeFrame")
public void setTimeFrame(Integer timeFrame) {
this.timeFrame = timeFrame;
}

@JsonProperty("percent")
public Integer getPercent() {
return percent;
}

@JsonProperty("percent")
public void setPercent(Integer percent) {
this.percent = percent;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}