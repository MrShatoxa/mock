package org.lanit.modelsJson;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"timeframe",
"percent"
})
@Generated("jsonschema2pojo")
public class Alert {

@JsonProperty("timeframe")
private Integer timeframe;
@JsonProperty("percent")
private Integer percent;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("timeframe")
public Integer getTimeframe() {
return timeframe;
}

@JsonProperty("timeframe")
public void setTimeframe(Integer timeframe) {
this.timeframe = timeframe;
}

@JsonProperty("percent")
public Integer getPercent() {
return percent;
}

@JsonProperty("percent")
public void setPercent(Integer percent) {
this.percent = percent;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}