package org.lanit.modelsJson;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"userID",
"tickers"
})
@Generated("jsonschema2pojo")
public class Info {

@JsonProperty("userID")
private String userID;
@JsonProperty("tickers")
private List<Ticker> tickers;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("userID")
public String getUserID() {
return userID;
}

@JsonProperty("userID")
public void setUserID(String userID) {
this.userID = userID;
}

@JsonProperty("tickers")
public List<Ticker> getTickers() {
return tickers;
}

@JsonProperty("tickers")
public void setTickers(List<Ticker> tickers) {
this.tickers = tickers;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}