package org.lanit.modelsJson;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"tickerName",
"alertIndex"
})
@Generated("jsonschema2pojo")
public class Delete {

@JsonProperty("tickerName")
private String tickerName;
@JsonProperty("alertIndex")
private Integer alertIndex;
@JsonIgnore
private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

@JsonProperty("tickerName")
public String getTickerName() {
return tickerName;
}

@JsonProperty("tickerName")
public void setTickerName(String tickerName) {
this.tickerName = tickerName;
}

@JsonProperty("alertIndex")
public Integer getAlertIndex() {
return alertIndex;
}

@JsonProperty("alertIndex")
public void setAlertIndex(Integer alertIndex) {
this.alertIndex = alertIndex;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
