package org.lanit.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class IncorrectAction extends RuntimeException{
    public IncorrectAction(String message) {
        super(message);
    }
}
