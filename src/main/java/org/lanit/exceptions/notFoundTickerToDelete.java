package org.lanit.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class notFoundTickerToDelete extends RuntimeException{
    public notFoundTickerToDelete(String message) {
        super(message);
    }
}