package org.lanit.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.tinylog.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.lanit.modelsJson.AddRequest;
import org.lanit.modelsJson.DeleteRequest;
import org.lanit.modelsJson.Ticker;
import org.lanit.modelsJson.Alert;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.lanit.exceptions.notFoundTickerToDelete;



@RestController
public class JSONController{

    @PostMapping(value = "json", params = "action=add")
    public Object AddResponse(@RequestBody String requestBody) throws IOException {
        int nonExistedTicker= 0;
        long startTime = System.currentTimeMillis();
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\templates\\json\\exampleResponse.json"), StandardCharsets.UTF_8);
        UUID uuid = UUID.randomUUID();
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        try{
            
            ObjectMapper  objectMapper = new ObjectMapper();
            ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

            AddRequest addRequest = objectMapper.readValue(requestBody, AddRequest.class);
            
            String userId = addRequest.getInfo().getUserID();
            List<Ticker> tickers = addRequest.getInfo().getTickers();
            
            String tickerName = addRequest.getAdd().getName();
            int alertTimeframe = addRequest.getAdd().getTimeFrame();
            int alertPercent = addRequest.getAdd().getPercent();
            
            Alert newAlert = new Alert();
            newAlert.setPercent(alertPercent);
            newAlert.setTimeframe(alertTimeframe);

            for (Ticker ticker : tickers) {
                if (ticker.getTicker().equals(tickerName)) {
                    ticker.getAlerts().add(newAlert);
                    nonExistedTicker++;
                };
                
            };
            
            if(nonExistedTicker==0){
                List<Alert> alertList = new ArrayList<>();
                alertList.add(newAlert);
            
                Ticker newTicker = new Ticker();
                newTicker.setTicker(tickerName); 
                newTicker.setAlerts(alertList);
                tickers.add(newTicker);  
            };
            
            String formatedDt = dtFormatter.format(dt);
            String tickersJsonFormat = objectWriter.writeValueAsString(tickers);
            String responseBody = String.format(templateResponse,userId,tickersJsonFormat,uuid,formatedDt);
            Logger.info(String.format("Заглушка отработала за - %s мс.UUID ответа - %s.",System.currentTimeMillis() - startTime, uuid));
            return ResponseEntity.ok().header("content-type", "application/json").body(responseBody);
        } catch(Exception e){
            Logger.error(String.format("%s\n%s",e.getMessage(), requestBody));
            return ResponseEntity.badRequest().header("content-type","application/json").body(String.format("{\"message\":\"Передана невалидная json\", \"request\":\"%s\"}",requestBody));
        }
    }

    @PostMapping(value = "json", params="action=delete")
    public Object DeleteResponse(@RequestBody String requestBody) throws IOException {
        int existedTickerToDelete= 0;
        long startTime = System.currentTimeMillis();
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\templates\\json\\exampleResponse.json"), StandardCharsets.UTF_8);
        UUID uuid = UUID.randomUUID();
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        try{
            ObjectMapper  objectMapper = new ObjectMapper();
            ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

            DeleteRequest deleteRequest = objectMapper.readValue(requestBody, DeleteRequest.class);

            String userId = deleteRequest.getInfo().getUserID();
            List<Ticker> tickers = deleteRequest.getInfo().getTickers();
            
            String deleteTickerName = deleteRequest.getDelete().getTickerName();
            int deleteAlertIndex = deleteRequest.getDelete().getAlertIndex();
            
            for (Ticker ticker : tickers) {
                if (ticker.getTicker().equals(deleteTickerName)) {
                    ticker.getAlerts().remove(deleteAlertIndex);
                    existedTickerToDelete++;
                };
            };
            if(existedTickerToDelete==0){
                throw new notFoundTickerToDelete("not found ticker with that name");
            }
            String formatedDt = dtFormatter.format(dt);
            String tickersJsonFormat = objectWriter.writeValueAsString(tickers);
            String responseBody = String.format(templateResponse,userId,tickersJsonFormat,uuid,formatedDt);
            Logger.info(String.format("Заглушка отработала за - %s мс.UUID ответа - %s.",System.currentTimeMillis() - startTime, uuid));
            return ResponseEntity.ok().header("content-type", "application/json").body(responseBody);
        } catch(Exception e){
            Logger.error(String.format("%s\n%s",e.getMessage(), requestBody));
            return ResponseEntity.badRequest().header("content-type","application/json").body(String.format("{\"message\":\"Передана невалидная json\", \"request\":\"%s\"}",requestBody));
        }
    }
    
}